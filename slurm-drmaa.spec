Name:		slurm-drmaa
Version:	1.0.7
Release:	1%{?dist}.4
Summary:	DRMAA for Slurm

Group:		System Environment/Base
License:	GPLv3
URL:		http://apps.man.poznan.pl/trac/slurm-drmaa
#Source0:	http://apps.man.poznan.pl/trac/slurm-drmaa/downloads/9
Source0:	%{name}-%{version}.tar.gz
BuildRoot:	%(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildRequires:	slurm-devel
Requires:	slurm

%description
PSNC DRMAA for  Simple Linux Utility for Resource Management (SLURM) is an
implementation of  Open Grid Forum  DRMAA 1.0 (Distributed Resource Management
Application API)  specification for submission and control of jobs to SLURM.
Using DRMAA, grid applications builders, portal developers and ISVs can use 
the same high-level API to link their software with different cluster/resource
management systems.

%package devel
Group: Development/Libraries
Summary: Files needed for developing apps that link to drmaa
Requires: %{name}%{?_isa} = %{version}-%{release}

%description devel
Development libraries and files for %{name}

%prep
%setup -q

%build
# galaxy people claim that optimization causes pthread related deadlocks, hm, fine, turn off optimization
export CFLAGS=`echo %{optflags} | sed 's/O2/O0/'`
%configure
make %{?_smp_mflags}

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_bindir}
%makeinstall
rm -f %{buildroot}%{_libdir}/libdrmaa.la
mv %{buildroot}/etc/slurm_drmaa.conf.example %{buildroot}/etc/slurm_drmaa.conf

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc README NEWS COPYING
%{_bindir}/*
%{_libdir}/libdrmaa.so.*
%config(noreplace) /etc/slurm_drmaa.conf

%files devel
%defattr(-,root,root,-)
%{_includedir}/drmaa.h
%{_libdir}/libdrmaa.so
%{_libdir}/libdrmaa.a

%changelog
* Tue Apr 21 2015 Josko Plazonic <plazonic@princeton.edu> 1.0.7-1.sdl6.1
- initial build
